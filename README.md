# Ansible Playbooks for ThisTech (CTS) Products

Ansible Playbook files for deploying CTS (p.k.a. ThisTech) products such as
Trajectory and SwitchStream.


## Requirements

1. These playbooks were written to work with Ansible version 2.1.6.0 and are known
to work with ansible 2.2.1.0.
2. Python 2.6.x is known to work. Python 2.7.x *may* work
3. ENVariables ```NEXUS_USERNAME``` & ```NEXUS_PASSWORD``` must be set to enable download of artifacts from the Nexus repo
    1. NOTE: To work properly with Ansible, ```NEXUS_USERNAME``` & ```NEXUS_PASSWORD``` must be set in _your_ .profile file.
    2. See example below


## Release Notes

* 20190610 [JPE] Numerous changes including update to the required version of Ansible 2.8.1

* 20171024 [JPE] Added this README and introduced use of ENVariables for Nexus access

## TODO

* Extract and Migrate customer and product specific configuration files to other git repositories.

## Deployment

1. Clone this repository (ansible) onto the "Ansible Server" machine.
  * The "Ansible Server" machine could be either SIE laptop or an environment/customer specific host
2. Clone the "inventory" (a.k.a. hosts) and group_vars/all files for the product and environment
  1. Copy the cloned inventory file to ansible directory
    1. symlink the cloned inventory (hosts) file to "hosts"
  2. Copy the cloned group_vars/all files to the "ansible/group_vars/" directory
    1. symlink the cloned file to "all"

### Verification

A "test_vars.yml" playbook file is included here, that may be run to verify
ENVariables ```NEXUS_USERNAME``` & ```NEXUS_PASSWORD``` are set properly.
These envariables are leveraged within the "group_vars/all" file

To verify the NEXUS_* envariables are set as needed, you can run the following command.

``ansible-playbook test_vars.yml``

The test_vars playbook will print several debug messages showing several Ansible variable values.

In the test_vars playbook example output below, one of the last
actions is failing failing *as expected*, since the ``BOGUS_ENVARIABLE`` is not set.

#### Setting NEXUS_USERNAME & NEXUS_PASSWORD ENVariables

Add lines similar to the following to your .profile file. 
Setting them in .bashrc does not work since Ansible is a batch system and avoids 
interactive logins if possible, which means it won't source certain user/shell specific files.

```bash
export NEXUS_USERNAME=yogi.bear
export NEXUS_PASSWORD=SO0pRCkr1t
```


Note `nexus_pass` and `nexus_user` are  Ansible variables used within
the playbooks to access the Nexus (maven) repository. Within the `group_vars/all`
file, those Ansible variables are set based on the environment variables.


```yaml
nexus_user: "{{ ansible_env.NEXUS_USERNAME }}"
nexus_pass: "{{ ansible_env.NEXUS_PASSWORD }}"
```


#### test_vars Playbook Example Output


```yaml
TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "ansible_env.NEXUS_USERNAME is yogi.bear"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "lookup('env', 'NEXUS_USERNAME') is yogi.bear"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "nexus_user yogi.bear"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "ansible_env.NEXUS_PASSWORD is SO0pRCkr1t"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "lookup('env', 'NEXUS_PASSWORD') is SO0pRCkr1t"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "nexus_pass is SO0pRCcr1t"
}

TASK [debug] *******************************************************************
ok: [127.0.0.1] => {
    "msg": "The following tasks are expected to fail - lookup('env', 'BOGUS_ENVARIABLE') is "
}

TASK [debug] *******************************************************************
fatal: [127.0.0.1]: FAILED! => {"failed": true, "msg": "the field 'args' has an invalid value, which appears to include a variable that is undefined. The error was: 'BOGUS_ENVARIABLE' is undefined

The error appears to have been in ' ansible/test_vars.yml': line 27, column 5, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

  - debug: msg=\"The following tasks are expected to fail - lookup('env', 'BOGUS_ENVARIABLE') is {{ lookup('env', 'BOGUS_ENVARIABLE') }}\"
  - debug: msg=\"BOGUS_ENVARIABLE is {{ BOGUS_ENVARIABLE }}\"
    ^ here
We could be wrong, but this one looks like it might be an issue with
missing quotes.  Always quote template expression brackets when they
start a value. For instance:

    with_items:
      - {{ foo }}

Should be written as:

    with_items:\n
      - \"{{ foo }}\"
"}
to retry, use: --limit @/Users/jeid200/Projects/ansible/test_vars.retry

PLAY RECAP *********************************************************************
127.0.0.1                  : ok=14   changed=0    unreachable=0    failed=1

```
