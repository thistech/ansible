The default mongod.service file installed via RPM must must be 
adjusted to suit the desired THISTECH directory structure.

The playbook has been updated to edit the RPM installed 
mongod.service file in /lib/systemd/system, AND the
"supplementary" unit file if it exists. 

The supplementary unit file takes precedence over the unit file
installed by the RPM.

After servicectl edit mongod.service --full, we end up with the
following differences between the "dist" version and the one 
we're interested in. 

$ diff /lib/systemd/system/mongod.service /etc/systemd/system/mongod.service
11,13c11,13
< ExecStartPre=/usr/bin/mkdir -p /var/run/mongodb
< ExecStartPre=/usr/bin/chown mongod:mongod /var/run/mongodb
< ExecStartPre=/usr/bin/chmod 0755 /var/run/mongodb
---
> #ExecStartPre=/usr/bin/mkdir -p /var/run/mongodb
> #ExecStartPre=/usr/bin/chown mongod:mongod /var/run/mongodb
> #ExecStartPre=/usr/bin/chmod 0755 /var/run/mongodb
15c15,16
< PIDFile=/var/run/mongodb/mongod.pid
---
> #PIDFile=/var/run/mongodb/mongod.pid
> PIDFile=/opt/mongodb-3.6/var/run/mongod.pid

	sed -e 's/^ExecStartPre/#ExecStartPre/' mongod.service.dist
	sed -E 's/^(ExecStartPre.*)$/#\1/' mongod.service.dist

	sed -e 's/^PIDFile.*/PIDFile=\/opt\/mongodb-3.6\/var\/run\/mongod.pid/' mongod.service.dist

	 sed -E 's/^(PIDFile=).*$/\1\/opt\/mongodb-3.6\/var\/run\/mongod.pid/' mongod.service.dist
