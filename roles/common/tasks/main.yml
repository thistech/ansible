---
- name: Verify Minimum Ansible Version
  assert:
    that: "{{ansible_version.full is version('2.8.1', '>=') }}"
    msg: >
      "Minimum version of Ansible to run these Trajectory playbooks is 2.8.1."

- name: install selinux bindings
  yum: 
    name: libselinux-python 
    state: present

- name: disable selinux
  selinux: 
    state: disabled

- name: create applications directory
  file: 
    path: /opt/applications 
    state: directory 
    mode: 0755

# TODO - add support for choosing which EPEL repo
# (Extra Packages for Enterprise Linux) to install depending on
# CentOS 6 vs CentOS 7
# when: ansible_distribution == 'CentOS'
# and ansible_distribution_version|int < 7
# when: ansible_distribution == 'CentOS'
# and ansible_distribution_version|int >= 7
# https://fedoraproject.org/wiki/EPEL
# 6 - yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
# 7 - yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Excluding Tomcat since we want the RPM from the THISTECH 3rd party repo
- name: Add CentOS 6 EPEL repository if distribution version LT 7
  yum_repository:
    name: epel
    description: EPEL YUM repo
    mirrorlist: http://mirrors.fedoraproject.org/mirrorlist?repo=epel-6&arch=$basearch
    exclude: tomcat*
    gpgcheck: no
    enabled: yes
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int < 7

# Excluding Tomcat since we want the RPM from the THISTECH 3rd party repo
- name: Add CentOS 7 EPEL repository if distribution version GE 7
  yum_repository:
    name: epel
    description: EPEL YUM repo
    mirrorlist: http://mirrors.fedoraproject.org/mirrorlist?repo=epel-7&arch=$basearch
    exclude: tomcat*
    gpgcheck: no
    enabled: yes
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int >= 7

- name: Add Thistech Repo if distribution version LT 7
  yum_repository:
    name: THIS
    description: This Technology CentOS 6 Specific Repository
    gpgcheck: no
    baseurl: http://yum.eng.thistech.com/repo/thirdparty
    enabled: yes
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int < 7

- name: Remove original Thistech Repo if distribution version GE 7
  file:
    name: /etc/yum.repos.d/THIS.repo
    state: absent
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int >= 7

- name: Add Thistech EL7 Repo
  yum_repository:
    name: THIS-EL7
    description: This Technology CentOS 7 Specific Repository
    gpgcheck: no
    baseurl: http://yum.eng.thistech.com/repo-el7/thirdparty
    enabled: yes
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int >= 7

- name: Add Thistech Distro Agnostic Repository
  yum_repository:
    name: THIS-NA
    description: This Technology Supplementary Repository
    gpgcheck: no
    baseurl: http://yum.eng.thistech.com/repo-na/thirdparty
    enabled: yes
  when: 
    ansible_distribution == 'CentOS' and
    ansible_distribution_version|int >= 7

- name: Add ElasticSearch repo
  yum_repository:
    name: ElasticSearch
    description: ElasticSearch
    gpgcheck: yes
    gpgkey: http://packages.elastic.co/GPG-KEY-elasticsearch
    baseurl: http://packages.elastic.co/elasticsearch/2.x/centos
    enabled: yes

- name: Add NGINX repo
  yum_repository:
    name: NGINX
    description: NGINX
    gpgcheck: no
    baseurl: http://nginx.org/packages/centos/$releasever/$basearch/
    enabled: yes

# Specifying jdk1.8.x86_64 for JDK_VERSION may not apply updates
# if JDK is already at 1.8
# e.g. from _162 to _181
- name: Install Common Rpms
  yum: 
    name: "{{ packages }}"
  vars:
    packages:
    - "{{JDK_VERSION}}"
    - ntp
    - zip
    - unzip
    - python-lxml
    - jq
    lock_timeout: 120

# Default nano is for newbies
- name: Set SYSTEMD_EDITOR to vi
  copy: 
    content: "export SYSTEMD_EDITOR=/bin/vi"
    dest: /etc/profile.d/systemd-editor.sh

- name: Set Java Home
  copy: 
    content: "export JAVA_HOME=$(j=$( readlink -f /usr/bin/java ) ; echo ${j%%/bin/java})" 
    dest: /etc/profile.d/java.sh

- name: set limits
  copy: 
    src: limits.conf 
    dest: /etc/security/limits.conf

- name: set nproc user limits
  copy: 
    src: 90-nproc.conf 
    dest: /etc/security/limits.d/90-nproc.conf

# The template used here _should_ be found in roles/common/templates
# https://github.com/ansible/ansible/blob/devel/docs/docsite/rst/user_guide/playbook_pathing.rst
# behavior may depend on version of Ansible
- name: Zookeeper config
  template: 
    src: templates/zookeeper.sh.j2 
    dest: /etc/profile.d/zookeeper.sh

# TODO - add support for checking if the service actually exists
#
# the newer CentOS 6.10 AMI doesn't include sendmail
# so this will fail when it attempts to stop the service
# The CentOS 7.6 AMI doesn't include any of these services,
# so failure will occur for all 3 iptables, ip6tables, sendmail
# References:
# https://www.middlewareinventory.com/blog/ansible-changed_when-and-failed_when-examples/
# https://stackoverflow.com/questions/30328506/check-if-service-exists-with-ansible
#
# Note - ignore_errors should prevent the play from failing completely on error
- name: Stop Unused services
  service: 
    name: "{{ item }}" 
    state: stopped 
    enabled: no
  register: command_result
  failed_when: "command_result is failed and 'no service or tool' not in command_result.msg"
  with_items:
    - iptables
    - ip6tables
    - sendmail
  ignore_errors: yes
  
