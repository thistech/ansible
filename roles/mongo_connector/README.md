# Ansible Playbook to deploy ThisTech (CTS) version of mongo-connector

The original deployment of mongo-connector used a pair of tar files
sourced from a THISTECH git repo. The latest revision of this 
playbook deploys the individual files of the implementation. 

The intent of the change is to clarify (prior to deployment) what
is included in the deployment, and pave the way for more flexibility
with the deployments. 

An example of the desired flexibility is to support deployment on a 
on a non-Elasticsearch node. The tarball approach assumed it was
co-located on a node with Elasticsearch.  

files/mongo-connector.tar was made directly from 
bitbucket.org/thistech/utilities/scripts/mongo-connector/

	tar -tvf files/mongo-connector.tar 2>/dev/null
	mongo-connector/
	mongo-connector/doc-manager/
	mongo-connector/doc-manager/doc-manager.tar
	mongo-connector/doc-manager/mongo_connector/
	mongo-connector/doc-manager/doc-manager.tar
	mongo-connector/doc-manager/mongo_connector/
	mongo-connector/doc-manager/mongo_connector/__init__.py
	mongo-connector/doc-manager/mongo_connector/doc_managers/
	mongo-connector/doc-manager/mongo_connector/doc_managers/__init__.py
	mongo-connector/doc-manager/mongo_connector/doc_managers/metamore_doc_manager.py
	mongo-connector/doc-manager/README.rst
	mongo-connector/doc-manager/setup.py
	mongo-connector/metamore/
	mongo-connector/metamore/config.properties
	mongo-connector/metamore/elasticsearch_dump_settings.json
	mongo-connector/metamore/elasticsearch_product_settings.json
	mongo-connector/metamore/mongo-connector-config.json
	mongo-connector/metamore/mongo-connector.tar
	mongo-connector/metamore/restore-elasticsearch-settings.sh
	mongo-connector/metamore/run-mongo-connector.sh
	mongo-connector/metamore/scl-run-mongo-connector.sh
	mongo-connector/metamore/status.html
	mongo-connector/metamore/sync-monitor.sh
	mongo-connector/monitor/
	mongo-connector/monitor/mcstatus.sh


files/elastic.tar was made directly from the contents of
bitbucket.org/thistech/utilities/scripts/elasticsearch/metamore/

	tar -tvf files/elastic.tar 2>/dev/null
	config.properties
	elasticsearch_mapping.json
	elasticsearch_settings.json
	elasticsearch_setup.sh
