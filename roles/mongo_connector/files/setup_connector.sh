#!/bin/bash
source /opt/rh/python27/enable

cd /opt/applications/mongo-connector

# 20190718 JPE - Commenting out the in-place edit since the latest 
# version of the mapping file no longer includes the _id mapping. 
# Remove the "_id": {"type": "string"}, property from the mapping file
#sed -i '5,7 d' elasticsearch_mapping.json

./elasticsearch_setup.sh

which python
which pip
pip install --upgrade pip
pip install 'mongo-connector==2.3'
pip install 'elastic2_doc_manager==0.2.0'

cd /opt/applications/mongo-connector/doc-manager
python setup.py install

# var/log directory moved under /opt/applications/mongo-connector
# and created by Ansible
# mkdir -p /opt/mongo-connector/var/log/
