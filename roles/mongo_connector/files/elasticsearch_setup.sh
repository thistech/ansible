#!/usr/bin/env bash
#
# file: elasticsearch_setup.sh
#
# Prepare the Elasticsearch cluster with the metamore index and mapping 
#

host=$(grep -E "^elasticsearch.host=.*$" config.properties | awk -F= '{print $2}');
port=$(grep -E "^elasticsearch.port=.*$" config.properties | awk -F= '{print $2}');

echo "Removing existing index mongodb_meta: curl -XDELETE $host:$port/mongodb_meta"
curl -XDELETE $host:$port/mongodb_meta
echo ""
echo ""

echo "Removing existing index metamore: curl -XDELETE $host:$port/metamore"
curl -XDELETE $host:$port/metamore
echo ""
echo ""

## Create index 'metamore'
echo "Creating index metamore: curl -XPUT $host:$port/metamore/ --data-binary @elasticsearch_settings.json"
curl -XPUT $host:$port/metamore/ --data-binary @elasticsearch_settings.json
echo ""
echo ""

## Create mapping for the type AssetContainer in the index 'metamore'
echo "Creating mapping for the index metamore: curl -XPUT $host:$port/metamore/_mapping/AssetContainer --data-binary @elasticsearch_mapping.json"
curl -XPUT $host:$port/metamore/_mapping/AssetContainer --data-binary @elasticsearch_mapping.json
echo ""
