#! /bin/bash
# Source function library.
. /etc/rc.d/init.d/functions
mc=/usr/bin/mongo-connector
prog=`/bin/basename $mc`
log_time=`date "+%x %X"`

# 
# failure_count_file: The file that records failure counts of restarting mongo-connector.
# failure_interval: The script will send an alert email after how many times restarting Mongo-Connector failed. Default is 5,
# which means script will send an alert email every 5 failures.
# 
failure_count_file=/var/log/failure.count
failure_interval=5

if [[ ! -e "$failure_count_file" ]]; then
   echo 0 > $failure_count_file
fi
RESTART_FAILURE=`cat $failure_count_file`

# MongoDB
mhost=localhost:27017

# ElasticSearch
eshost=172.31.1.254:9200

# Path to store Mongo-Connector log
mclogfile=/var/log/mc.log

# Path to store monitor log
monitorlog=/var/log/mc-monitor.log

# Mail options
subject="Mongo-Connector stopped and has been started by monitoring script."
recipient="yan.jiao@thistech.com"

RETVAL=0

start() {
    mongo-connector -m ${mhost} -t ${eshost} -d elastic_doc_manager --auto-commit-interval=0 -n metamore.AssetContainer --verbose --logfile ${mclogfile} --logfile-when=D --logfile-interval=1 --logfile-backups=7 &
    RETVAL=$?
    if [ $RETVAL -gt 0 ]; then
       RESTART_FAILURE=$(($RESTART_FAILURE+1))
       if [ `expr $RESTART_FAILURE % $failure_interval` = 0 ]; then
          subject="Restarting Mongo-Connector has failed ${RESTART_FAILURE} times."
       fi
       echo $RESTART_FAILURE > $failure_count_file
    else
       echo 0 > $failure_count_file
    fi
    echo "$log_time  Mongo-Connector stopped and started by monitoring script." >> ${monitorlog}; tail -n 30 ${mclogfile} | mailx -s "${subject}" ${recipient}
    return $RETVAL
}

procnum=`/bin/ps aux| /bin/grep mongo-connector | /bin/grep -v grep | /usr/bin/wc -l`

# Start Mongo-Connector, if the process doesn't exist.
if [ $procnum -eq 0 ]; then
        start > /dev/null
fi
exit $RETVAL

