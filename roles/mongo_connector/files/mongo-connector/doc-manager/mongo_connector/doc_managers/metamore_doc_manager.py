# Copyright 2013-2014 MongoDB, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Elasticsearch implementation of the DocManager interface for MetaMore only.

Receives documents from an OplogThread and takes the appropriate actions on
Elasticsearch.
"""
import SimpleHTTPServer
import SocketServer
import logging
import socket
import threading
import time

from elasticsearch import Elasticsearch, exceptions as es_exceptions

from mongo_connector import errors
from mongo_connector.constants import (DEFAULT_COMMIT_INTERVAL,
                                       DEFAULT_MAX_BULK)
from mongo_connector.util import exception_wrapper
from mongo_connector.doc_managers.elastic2_doc_manager import DocManager as ElasticDocManager
from mongo_connector.doc_managers.formatters import DefaultDocumentFormatter
wrap_exceptions = exception_wrapper({
    es_exceptions.ConnectionError: errors.ConnectionFailed,
    es_exceptions.TransportError: errors.OperationFailed,
    es_exceptions.NotFoundError: errors.OperationFailed,
    es_exceptions.RequestError: errors.OperationFailed})

LOG = logging.getLogger(__name__)


class DocManager(ElasticDocManager):
    """Elasticsearch implementation of the DocManager interface.

    Receives documents from an OplogThread and takes the appropriate actions on
    Elasticsearch.
    """

    def __init__(self, url, auto_commit_interval=DEFAULT_COMMIT_INTERVAL,
                 unique_key='_id', chunk_size=DEFAULT_MAX_BULK,
                 meta_index_name="mongodb_meta", meta_type="mongodb_meta",
                 attachment_field="content", **kwargs):
        LOG.info("metamore_doc_manager is used: " + str(url))
        hosts = []
        if isinstance(url, str):
            hosts.append(url)
        elif isinstance(url, list):
            hosts = url
        self.elastic = Elasticsearch(hosts, **kwargs.get('clientOptions', {}))
        self.auto_commit_interval = auto_commit_interval
        self.meta_index_name = meta_index_name
        self.meta_type = meta_type
        self.unique_key = unique_key
        self.chunk_size = chunk_size
        if self.auto_commit_interval not in [None, 0]:
            self.run_auto_commit()
        self._formatter = DefaultDocumentFormatter()

        self.has_attachment_mapping = False
        self.attachment_field = attachment_field

        self.statusServer = StatusServerThread(**kwargs.get('statusServer', {"port": 5669}))
        self.statusServer.setDaemon(True)
        self.statusServer.start()


class StatusServerThread(threading.Thread):
    def __init__(self, port=5669, restart_count=1):
        super(StatusServerThread, self).__init__()
        self.port = port
        self.name = "StatusServer-" + str(restart_count)
        self.restart_count = restart_count
        self.server = None

    def run(self):
        try:
            print self.name + ": StatusServerThread has been started. serve at port " + str(self.port) + "\n"
            LOG.info(self.name + ": StatusServerThread has been started. serve at port " + str(self.port))
            self.server = SocketServer.TCPServer(("", self.port), SimpleHTTPServer.SimpleHTTPRequestHandler)
            self.server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.server.serve_forever()
        except Exception, e:
            LOG.error(self.name + ": Exception occurred! %s\n" % e)
        finally:
            if self.server is not None:
                LOG.info(self.name + ": Shutting down status server")
                self.server.server_close()
                self.server.shutdown()
            time.sleep(10)
            self.restart_server()
            LOG.info(self.name + ": StatusServer has been stopped unexpectedly!")

    def restart_server(self):
        LOG.info(self.name + ": restarting status server")
        count = self.restart_count if self.server is None else self.restart_count + 1
        new_status_server = StatusServerThread(self.port, count)
        new_status_server.setDaemon(True)
        new_status_server.start()
