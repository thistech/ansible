#!/bin/sh
############ count doc num in mongo #################
res=$(mongo -umetamore -pmetamore metamore << EOF
print("COUNT:"+db.AssetContainer.count())
exit
EOF
)
mongocount=$(echo $res | grep -Eo "COUNT:[0-9]+")
mongonum=$(echo ${mongocount#COUNT:})

########### count doc num in elasticsearch #########
function countelastic()
{
	rawIP=$(ifconfig eth0 | grep -Eo "inet addr:[^ ]+");
	localIP=${rawIP#inet addr:};
	elasticnum=$(curl -XGET "http://$localIP:9200/_cat/indices?v&pretty" | awk '/mongodb_meta/ {print $6;}');
	echo $elasticnum
}
echo "Mongo doc num is: "$mongonum
#elasticnum=$(countelastic)
#echo "elasticnum is: [$elasticnum]."


while true
do
	elasticnum=$(countelastic)
	echo "elasticnum is: [$elasticnum]."
	if [ -z $elasticnum ]
	then
		sleep 10
		continue
	fi
	
	if [ $elasticnum -lt $mongonum ]
	then
		echo "Still syncing..."
		sleep 10
	else
		break
	fi
done
echo "Sync has been done!"
	
