#!/usr/bin/env bash
BASEDIR=/opt/applications/mongo-connector
host=$(grep -E "^elasticsearch.host=.*$" ${BASEDIR}/config.properties | awk -F= '{print $2}');
port=$(grep -E "^elasticsearch.port=.*$" ${BASEDIR}/config.properties | awk -F= '{print $2}');

##After dump is done, run the following command to recover settings for the index 'metamore'
echo "Restore settings: curl -XPUT $host:$port/metamore/_settings?pretty --data-binary @elasticsearch_product_settings.json"
curl -XPUT $host:$port/metamore/_settings?pretty --data-binary @elasticsearch_product_settings.json
echo ""
