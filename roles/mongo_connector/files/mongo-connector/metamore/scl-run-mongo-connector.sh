#!/usr/bin/env bash
BASEDIR=/opt/applications/mongo-connector
host=$(grep -E "^elasticsearch.host=.*$" ${BASEDIR}/config.properties | awk -F= '{print $2}');
port=$(grep -E "^elasticsearch.port=.*$" ${BASEDIR}/config.properties | awk -F= '{print $2}');
dbAddress=$(grep "\"mainAddress\"" ${BASEDIR}/metamore/mongo-connector-config.json | awk -F\" '{print $4}');
logFile=$(grep "\"logging\"" -A 5 ${BASEDIR}/metamore/mongo-connector-config.json | grep "\"filename\"" | awk -F\" '{print $4}');

if [ "$1" == "-d" ]; then
  ##Disable the merge and refresh , so the dump process can be faster.
  echo "Update elasticsettings for dumping data: curl -XPUT $host:$port/metamore/_settings?pretty --data-binary @elasticsearch_dump_settings.json"
  curl -XPUT $host:$port/metamore/_settings?pretty --data-binary @elasticsearch_dump_settings.json
  echo ""
  echo ""
fi

## Mongo-connector checks if the oplog.rs has a document for the metamore.AssetContainer, and it will not dump data if there
## is no documents found in oplog.rs. So we insert and then immediately delete a document to the metamore.AssetContainer collection
## to ensure the oplog.rs will have a document for metamore.AssetContainer before the Mongo-connector starts to dump data.
#mongo $dbAddress --eval "db.getSisterDB('metamore').getCollection('AssetContainer').insert({_id:'temporary_unique_id_for_mongo_connector'})"
#mongo $dbAddress --eval "db.getSisterDB('metamore').getCollection('AssetContainer').remove({_id:'temporary_unique_id_for_mongo_connector'})"

# Note mongo-connector must be run via the scl (Software Colletion environment) command to
# use the correct version of Python (i.e. 2.7.x).
# When starting manually from the command line, issuing the command
# 'scl enable python27 bash' will spawn a new shell with the correct Python version.
# Baking the 'scl' command into this script with a "heredoc" eliminates the 2 step
# process to start mongo-connector
#=======================
# Original - requires scl first
#Run the following command to start dump data from mongo to elasticsearch
#echo "Start mongo-connector: mongo-connector -c mongo-connector-config.json &"
#mongo-connector -c mongo-connector-config.json >> $logFile 2>&1 &
#-----------------------
scl enable python27 - << \EOF
cd /opt/applications/mongo-connector/metamore
logFile=$(grep "\"logging\"" -A 5 mongo-connector-config.json | grep "\"filename\"" | awk -F\" '{print $4}');
mongo-connector -c mongo-connector-config.json >> $logFile 2>&1 &
EOF

# For automation purpose, monitoring if docs.count of mongo_meta is same to metamore.AssetContainer.count()
# If sync done, following script will quit with exit 0
if [ "$2" == "test" ]
then
        ./sync-monitor.sh
fi

