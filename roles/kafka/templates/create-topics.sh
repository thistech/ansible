#!/bin/sh

. /etc/profile.d/zookeeper.sh

#SLR-NextGen Topics

# 345,600,000 milliSeconds = 4 days
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-auditing --config retention.ms=345600000
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-metrics --config retention.ms=345600000

# 259,200,000  milliSeconds = 3 days
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-incoming-psn --config retention.ms=259200000
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-incoming-psn-2 --config retention.ms=259200000
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-health-check --config retention.ms=259200000

/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic logging --config retention.ms=259200000
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic oplog-metrics --config retention.ms=259200000

# Default retention - set via /opt/kafka/config/server.properties - log.retention.hours=168 (7 days)  
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic cis-asset-events
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic content-filter-events
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic decision-owner-events
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic fallback-pool-events
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic slr-tracking-events
