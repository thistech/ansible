#!/bin/sh

. /etc/profile.d/zookeeper.sh

# SwitchStream 3.0 Topics
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic schedule
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic event
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic acs-metrics
/opt/kafka/bin/kafka-topics.sh --create --zookeeper $ZK_ENSEMBLE --replication-factor 2 --partitions 8 --topic acquisitionSystemProfile
