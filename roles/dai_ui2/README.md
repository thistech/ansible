# Deploy a second, separate copy of DAI-UI

This role supports deploying separate instances of DAI-UI on a different set of machines for cases where a dual ADM setup is supported.

The first example of a dual ADM configuration is at Verizon where they want to run both OpenStream and Fulcrum ADMs. 


## Requirements

Requirements for this project.

1. The "hosts" file lists a separate, distinct set of hosts which MUST be different than the "first" DAI-UI deployments.
2. A secondary copy of the spotbuilder database is available. 


## Release Notes

### DAI-UI 3.1.0-RC3

The first DAI-UI release to support a dual ADM configuration. 

Note the second copy of the UI is accessed via the same application path as the instance(s) for the first spotbuilder.

http://<somehost>:8080/dai-ui

Since GWT relies on the values in dai-ui-defaults.properties which is baked into the WAR file (dai-ui/WEB-INF/classes/dai-ui-defaults.properties), that file must be present and properly configured (i.e. match the values stored in Zookeeper).

Theoretically a copy of the dai-ui-defaults.properties file deployed in 
<CATALINA_HOME>/lib should over-ride the copy in dai-ui/WEB-INF/classes


