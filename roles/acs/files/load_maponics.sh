#!/bin/bash
for f in "."/*.kml 
do 
	filename=$(basename $f)
	echo "Processing : $filename ....";
	curl -vv -X POST -H Content-Type:application/xml -d @$filename --user $1:$2 http://$3/emp/data/kml/
	sleep 5;
done
